jQuery(document).ready(function ($) {
	"use strict";
	$(".pridesys-carousel").each(function (index) {
		var optionCarousel = $(this); //check this block
		var auto_play = Boolean(optionCarousel.attr('data-autoplay') === 'false' ? false : true);
		var loop = Boolean(optionCarousel.attr('data-loop') === 'false' ? false : true);
		var data_dots = Boolean(optionCarousel.attr('data-dots') === 'false' ? false : true);
		var navigaion = Boolean(optionCarousel.attr('data-nav') === 'true' ? true : false);

		var itemsCountDesktop = parseInt(optionCarousel.attr('data-large-items'));
		var itemsCountMedium = parseInt(optionCarousel.attr('data-medium-items'));
		var itemsCountSmall = parseInt(optionCarousel.attr('data-small-items'));
		var itemsCountXsmall = parseInt(optionCarousel.attr('data-xsmall-items'));

		var itemMargin = parseInt(optionCarousel.attr('data-margin'));

		optionCarousel.owlCarousel({
			autoplay: auto_play,
			loop: loop,
			dots: data_dots,
			margin: itemMargin,
			rewind: true,
			autoplayHoverPause: true,
			nav: navigaion,
			navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
			responsive: {
				0: {
					items: itemsCountXsmall,
				},
				768: {
					items: itemsCountSmall,
				},
				992: {
					items: itemsCountMedium,
				},
				1200: {
					items: itemsCountDesktop,
				}
			},
		});
	});

	/** =====================================
	* Counter
	* =====================================***/
	if ($('.counter-active').length) {
		$('.counter-active').counterUp({
			delay: 10,
			time: 1000
		});
	}

	/** =====================================
  * Slick
  * =====================================***/
	$('.testimonial-carousel').slick({
		infinite: true,
		prevArrow: false,
		nextArrow: false,
		slidesToShow: 1,
		dots: true,
		vertical: true,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					// infinite: true,
					// dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	/** =====================================
    *  Back to top
    * ===================================== **/
	$(window).scroll(function () {
		if ($(this).scrollTop() > 10) {
			$('#toTop').addClass('backtop-top-show');
		} else {
			$('#toTop').removeClass('backtop-top-show');
		}
	})
	$("#toTop").on('click', function (e) {
		e.preventDefault();
		$("html, body").animate({ scrollTop: 0 }, 1000);
	});


	/** =====================================
	*  Nav Menu
	* ===================================== **/
	$(function () {
		$(window).resize();
	});
	if($('#menu').length){

	
	$('#menu').dmenu({
		menu: {
			logo: true,
			align: 'right'
		},
		item: {
			bg: true,
			border: false,
			subindicator: true,

			fit: [
				{
					items: null,
					fitter: 'icon-hide',
					order: 'all'
				},
				{
					items: null,
					fitter: 'icon-only',
					order: 'all'
				},
				{
					items: ':not(.dm-item_align-right)',
					fitter: 'submenu',
					order: 'rtl'
				},
				{
					items: ':not(.dm-item_align-right)',
					fitter: 'hide',
					order: 'rtl'
				}
			]
		},
		submenu: {
			arrow: false,
			border: false,
			shadow: true
		},
		subitem: {
			bg: true,
			border: false
		}

	});
}
	/** =====================================
*   Search Box
* =====================================**/
	$('.search-box .search-icon').on('click', function (e) {
		e.preventDefault();
		$('.top-search-input-wrap').addClass('show');
	});
	$(".top-search-input-wrap .top-search-overlay, .top-search-input-wrap .close-icon").on('click', function () {
		$('.top-search-input-wrap').removeClass('show');
	});
	/** =====================================
*  Wow JS
* ===================================== **/
	if ($('.wow').length) {
		var wow = new WOW({
			boxClass: 'wow', // animated element css class (default is wow)
			animateClass: 'animated', // animation css class (default is animated)
			offset: 0, // distance to the element when triggering the animation (default is 0)
			mobile: false, // trigger animations on mobile devices (default is true)
			live: true, // act on asynchronously loaded content (default is true)
			callback: function (box) { }
			, scrollContainer: true // optional scroll container selector, otherwise use window
		}
		);
		wow.init();
	}


$("#thjmf_apply_now").click(function(e){
	$(".thjmf-popup-header .pop-up-title").remove();
	$(".thjmf-popup-header").append(`<span class="pop-up-title">${$('.blog-details-page-title').html()}</span>`);
})













});
